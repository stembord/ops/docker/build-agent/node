# stembord/build-agent-node:${NODE_VERSION}

FROM stembord/docker-kube-helm

ARG NODE_VERSION=10

RUN apt install -y \
  apt-transport-https \
  build-essential \
  ca-certificates \
  curl \
  git \
  libssl-dev \
  wget

ENV NVM_DIR /usr/local/nvm

WORKDIR $NVM_DIR

RUN curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash && \
  . $NVM_DIR/nvm.sh && \
  ln -s $NVM_DIR/versions/node/$(node -v) $NVM_DIR/versions/node/v$NODE_VERSION

ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH