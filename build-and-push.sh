#!/bin/bash

repo="stembord/build-agent-node"

function build_and_push() {
  local version=$1
  docker build --build-arg NODE_VERSION=${version} -t ${repo}:${version} .
  docker push ${repo}:${version}
}

build_and_push "8"
build_and_push "10"
build_and_push "11"
build_and_push "12"